# material-awesome

Material and Mouse driven theme for AwesomeWM 4.3

Installation

1) Get all the dependencies

Arch-Based

```
yay -S awesome rofi picom i3lock-fancy xclip ttf-roboto gnome-polkit materia-gtk-theme lxappearance flameshot pnmixer network-manager-applet xfce4-power-manager -y
wget -qO- https://git.io/papirus-icon-theme-install | sh
```


Program list
`AwesomeWM` as the window manager - universal package install: awesome
`Roboto`as the font - Debian: fonts-roboto Arch: ttf-roboto
`Rofi` for the app launcher - universal install: rofi
`picom` for the compositor (blur and animations) universal install: picom -
`i3lock` the lockscreen application universal install: i3lock-fancy
`xclip` for copying screenshots to clipboard package: xclip
`[gnome-polkit]` recommend using the gnome-polkit as it integrates nicely for elevating programs that need root access
`Materia as GTK theme` - Arch Install: materia-theme debian: materia-gtk-theme
`Papirus Dark` as icon theme Universal Install: wget -qO- https://git.io/papirus-icon-theme-install | sh
`lxappearance` to set up the gtk and icon theme
`xbacklight` for adjusting brightness on laptops (disabled by default)
`flameshot` my personal screenshot utility of choice, can be replaced by whichever you want, just remember to edit the apps.lua file
`pnmixer` Audio Tray icon that is in debian repositories and is easily installed on arch through AUR.
`network-manager-applet nm-applet` is a Network Manager Tray display from GNOME.
`xfce4-power-manager` XFCE4's power manager is excellent and a great way of dealing with sleep, monitor timeout, and other power management features.

2) Clone the configuration

`git clone https://gitlab.com/fouad-dev/material-awesome.git ~/.config/awesome`

3) Set the themes
Start lxappearance to active the icon theme and GTK theme Note: for cursor theme, edit ~/.icons/default/index.theme and `~/.config/gtk3-0/settings.ini`, for the change to also show up in applications run as root, copy the 2 files over to their respective place in `/root`.

4) Same theme for Qt/KDE applications and GTK applications, and fix missing indicators
First install qt5-styleplugins (arch) and add this to the bottom of your `/etc/environment`

XDG_CURRENT_DESKTOP=Unity
QT_QPA_PLATFORMTHEME=gtk2
The first variable fixes most indicators (especially electron based ones!), the second tells Qt and KDE applications to use your gtk2 theme set through lxappearance.

